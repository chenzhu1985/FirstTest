package MultiTask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MyExcutor {

	public void runTask() throws InterruptedException, ExecutionException {
		ExecutorService service = Executors.newFixedThreadPool(3);
		
		List<Future<String>> futures = new ArrayList<>();
		
		for (int i = 0; i < 10; i++) {
			MyCall myCall = new MyCall("name:" + i);
			Future<String> f = service.submit(myCall);
			futures.add(f);
		}
		
//		List<MyCall> tasks = new ArrayList<MyCall>();
//		for (int i = 0; i < 10; i++) {
//			MyCall myCall = new MyCall("name:" + i);
//			tasks.add(myCall);
//		}
//		List<Future<String>> futures = service.invokeAll(tasks);
		
		for (int i = 0; i < futures.size(); i++) {
			System.out.println(futures.get(i).get());
		}
		
		service.shutdown();
		
//		System.out.println(f[2].get());
	}
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		new MyExcutor().runTask();
	}
}
