package MultiTask;

import java.util.concurrent.Callable;

public class MyCall implements Callable<String>{

	String name;
	
	public MyCall(String name){
		this.name = name;
	}
	
	@Override
	public String call() throws Exception {
		System.out.println("Thread.currentThread().getName():"+Thread.currentThread().getName() + "   name:" + name);
		return name;
	}

}
