package lock;

public class LockRun implements Runnable{

	Lock lock = null;
	
	int idx = 0;
	
	public LockRun(Lock lock, int idx) {
		this.lock = lock;
		this.idx = idx;
	}
	
	@Override
	public void run() {
		try {
			if(idx == 1){
				lock.lock();
			}else if(idx == 2){
				lock.unlock();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
