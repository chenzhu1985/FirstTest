package lock;

public class Lock {
	
	private boolean isLocked = false;
	
	private Thread lockingThread = null;
	
	public synchronized void lock() throws InterruptedException{
		while (isLocked) {
			wait();
		}
		isLocked = true;
		
		lockingThread = Thread.currentThread();
	}
	
	public synchronized void unlock(){
//		if(this.lockingThread != Thread.currentThread()){
//			throw new IllegalMonitorStateException("123123123");
//		}
		isLocked = false;
		
		lockingThread = null;
		
		notify();
	}
	
	
	public static void main(String[] args) {
		Lock lock = new Lock();
		
		Thread thread1 = new Thread(new LockRun(lock,1),"1");
		
		Thread thread2 = new Thread(new LockRun(lock,1),"2");
		
		Thread thread3 = new Thread(new LockRun(lock,2),"3");
		
		thread1.start();
		
		thread2.start();
		
		thread3.start();
	}
}
