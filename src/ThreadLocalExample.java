
public class ThreadLocalExample {
	
	public static class MyRunnable implements Runnable{
		private ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>();
		
		private Integer xyz = new Integer(0);

		@Override
		public void run() {
			threadLocal.set((int) (Math.random() * 100D));
			
			add(100);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			System.out.println("threadLocal.get()=" + threadLocal.get());
			System.out.println(xyz);
		}
		
		public synchronized void add(int value){
			xyz += value;
			threadLocal.set(threadLocal.get() + value);
		}
		
	}
	
	
	public static void main(String[] args) throws InterruptedException {
		MyRunnable myRunnable = new MyRunnable();
		
		Thread thread1 = new Thread(myRunnable);
		
		Thread thread2 = new Thread(myRunnable);
		
		thread1.start();
		
		thread2.start();
		
		thread1.join();
		thread2.join();
	}
}
